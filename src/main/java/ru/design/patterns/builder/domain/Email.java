package ru.design.patterns.builder.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

import java.util.List;

/**
 * Email.
 *
 * @author Tatyana_Dolnikova
 */
@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Email {

    String subject;
    String sender;
    List<String> emailReceivers;
    List<String> copyReceivers;
    Content content;

}
