package ru.design.patterns.builder;

import static java.util.Arrays.asList;

import ru.design.patterns.builder.builder.EmailBuilder;
import ru.design.patterns.builder.domain.Content;
import ru.design.patterns.builder.domain.Email;

/**
 * Пример использования паттерна Builder.
 *
 * @author Tatyana_Dolnikova
 */
public class App {

    public static void main(String[] args) {
        Content content = new Content("Body", "Signature");
        Email email = new EmailBuilder()
                .subject("Very important email")
                .from("email@gmail.com")
                .to("emailReceiver1")
                .to("emailReceiver2")
                .toAll(asList("emailReceiver1", "emailReceiver2", "emailReceiver"))
                .copyTo("copyReceiver1")
                .copyTo("copyReceiver2")
                .copyToAll(asList("copyReceiver1", "copyReceiver2", "copyReceiver"))
                .content(content)
                .build();
        System.out.println(email);
    }

}
