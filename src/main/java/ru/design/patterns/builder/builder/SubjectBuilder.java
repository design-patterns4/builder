package ru.design.patterns.builder.builder;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

/**
 * SubjectBuilder.
 *
 * @author Tatyana_Dolnikova
 */
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SubjectBuilder {

    final String subject;

    public EmailReceiverBuilder from(String sender) {
        return new ReceiverBuilderImpl(subject, sender);
    }
}