package ru.design.patterns.builder.builder;

/**
 * EmailReceiverBuilder.
 *
 * @author Tatyana_Dolnikova
 */
public interface ReceiverBuilder extends EmailReceiverBuilder, CopyReceiverBuilder {
}
