package ru.design.patterns.builder.builder;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import ru.design.patterns.builder.domain.Content;

import java.util.ArrayList;
import java.util.List;

/**
 * ReceiverBuilderImpl.
 *
 * @author Tatyana_Dolnikova
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ReceiverBuilderImpl implements ReceiverBuilder {

    final String subject;
    final String sender;
    final List<String> emailReceivers;
    final List<String> copyReceivers;

    public ReceiverBuilderImpl(String subject, String sender) {
        this.subject = subject;
        this.sender = sender;
        this.emailReceivers = new ArrayList<>();
        this.copyReceivers = new ArrayList<>();
    }

    @Override
    public ReceiverBuilder to(String receiver) {
        addEmailReceiver(receiver);
        return this;
    }

    @Override
    public ReceiverBuilder toAll(List<String> receivers) {
        receivers.forEach(this::addEmailReceiver);
        return this;
    }

    private void addEmailReceiver(String receiver) {
        if (!emailReceivers.contains(receiver)) {
            emailReceivers.add(receiver);
        }
    }

    @Override
    public CopyReceiverBuilder copyTo(String receiver) {
        addCopyReceiver(receiver);
        return this;
    }

    @Override
    public CopyReceiverBuilder copyToAll(List<String> receivers) {
        receivers.forEach(this::addCopyReceiver);
        return this;
    }

    private void addCopyReceiver(String receiver) {
        if (!copyReceivers.contains(receiver)) {
            copyReceivers.add(receiver);
        }
    }

    @Override
    public ContentBuilder content(Content content) {
        return new ContentBuilder(subject, sender, emailReceivers, copyReceivers, content);
    }
}
