package ru.design.patterns.builder.builder;

import java.util.List;

/**
 * EmailReceiverBuilder.
 *
 * @author Tatyana_Dolnikova
 */
public interface EmailReceiverBuilder {

    ReceiverBuilder to(String receiver);

    ReceiverBuilder toAll(List<String> receivers);

}
