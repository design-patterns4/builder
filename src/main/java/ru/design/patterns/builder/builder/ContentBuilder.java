package ru.design.patterns.builder.builder;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import ru.design.patterns.builder.domain.Content;
import ru.design.patterns.builder.domain.Email;

import java.util.List;

/**
 * ContentBuilder.
 *
 * @author Tatyana_Dolnikova
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ContentBuilder {

    final String subject;
    final String sender;
    final List<String> emailReceivers;
    final List<String> copyReceivers;
    final Content content;

    public ContentBuilder(String subject, String sender, List<String> emailReceivers, List<String> copyReceivers, Content content) {
        this.subject = subject;
        this.sender = sender;
        this.emailReceivers = emailReceivers;
        this.copyReceivers = copyReceivers;
        this.content = content;
    }

    public Email build() {
        Email email = new Email();
        email.setSubject(subject);
        email.setSender(sender);
        email.setEmailReceivers(emailReceivers);
        email.setCopyReceivers(copyReceivers);
        email.setContent(content);
        return email;
    }
}
