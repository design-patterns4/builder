package ru.design.patterns.builder.builder;

import ru.design.patterns.builder.domain.Content;

import java.util.List;

/**
 * EmailReceiverBuilder.
 *
 * @author Tatyana_Dolnikova
 */
public interface CopyReceiverBuilder {

    CopyReceiverBuilder copyTo(String receiver);

    CopyReceiverBuilder copyToAll(List<String> receivers);

    ContentBuilder content(Content content);

}
