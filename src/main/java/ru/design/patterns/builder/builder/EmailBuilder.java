package ru.design.patterns.builder.builder;

/**
 * EmailBuilder.
 *
 * @author Tatyana_Dolnikova
 */
public class EmailBuilder {

    public SubjectBuilder subject(String subject) {
        return new SubjectBuilder(subject);
    }

}
